var service = require("../service/reports.service");
module.exports = {
    create_post: create_post,
    get_posts: get_posts,
    delete_post: delete_post,
    edit_post: edit_post,
    get_post_by_id: get_post_by_id
}

function get_post_by_id(req, res) {
    service.get_post_by_id(req.params).then((result) => {
        res.send(result);
    }).catch((err) => {
        res.send(err);
    });
}

function create_post(req, res) {
    service.create_post(req.body).then((result) => {
        res.send(result);
    }).catch((err) => {
        res.send(err);
    });
}

function edit_post(req, res) {
    service.edit_post(req.params, req.body).then((result) => {
        res.send(result);
    }).catch((err) => {
        res.send(err);
    });
}

function get_posts(req, res) {
    service.get_posts().then((result) => {
        res.send(result);
    }).catch((err) => {
        res.send(err);
    });
}


function delete_post(req, res) {
    service.delete_post(req.params).then((result) => {
        res.send(result);
    }).catch((err) => {
        res.send(err);
    });
}