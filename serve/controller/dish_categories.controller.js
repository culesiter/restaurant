var service = require("../service/dish_categories.service");
module.exports = {
    create_cat: create_cat,
    get_cat_list: get_cat_list,
    delete_cat: delete_cat1,
    update_cat: update_cat1,
}


function create_cat(req, res) {
    service.create_cat(req.body).then((result) => {
        res.send(result);
    }).catch((err) => {
        res.send(err)
    });
}

function update_cat1(req, res) {
    service.update_cat(req.params, req.body).then((result) => {
        res.send(result);
    }).catch((err) => {
        res.send(err);
    });
}

function get_cat_list(req, res) {
    service.get_cat_list().then((result) => {
        res.send(result);
    }).catch((err) => {
        res.send(err);
    });
}



function delete_cat1(req, res) {
    service.delete_cat(req.params).then((result) => {
        res.send(result);
    }).catch((err) => {
        res.send(err);
    });
}