var mongoose = require("mongoose");
var dishCategorySchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        require: true
    },
    name: {
        type: String,
        default: 'Loại món ăn mới'
    },
    description: {
        type: String,
        default: ''
    }

});
var dishCategories = mongoose.model('dishCategories', dishCategorySchema);
module.exports = dishCategories;