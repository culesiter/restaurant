var mongoose = require("mongoose");
var phongSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {
        type: String,
        require: true
    },
    content: {
        type: String
    },
    thumbnail_url: {
        type: String
    },
    created: {
        type: String
    },
});
var posts = mongoose.model('posts', phongSchema);
module.exports = posts;