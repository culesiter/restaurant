var mongoose = require("mongoose");
var phongSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    phone: {
        type: String,
        require: true
    },
    name: {
        type: String
    },
    content: {
        type: String
    },
    created: {
        type: String
    },
});
var reports = mongoose.model('reports', phongSchema);
module.exports = reports;