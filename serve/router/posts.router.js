var router = require("express").Router();
var controller = require("../controller/posts.controller");
module.exports = function() {
    router.post('/', controller.create_post);
    router.get('/', controller.get_posts);
    router.get('/:id', controller.get_post_by_id);
    router.delete('/:id', controller.delete_post);
    router.put('/:id', controller.edit_post);
    return router;
}