var router = require("express").Router();
var controller = require("./../controller/dish_categories.controller");
var multer = require('multer')
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './upload/monan/');
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname)
    }
})
const fileFiter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === ('image/png')) {
        cb(null, true)
    } else {
        cb(null, false)
    }
}

const upload = multer({ storage: storage, fileFiter: fileFiter })

module.exports = function() {
    router.post('/', controller.create_cat);
    router.get('/', controller.get_cat_list);
    router.delete('/:id', controller.delete_cat);
    router.put('/:id', controller.update_cat);
    return router;
}