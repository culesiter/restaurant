const monan = require("./../model/monan.model");
const loaimon = require("./../model/loaimonan.model")
const dishcategories = require("./../model/dish_categories.model")
const mongoose = require("mongoose")
module.exports = {
    create_cat: create_cat,
    get_cat_list: get_cat_list,
    delete_cat: delete_cat,
    update_cat: update_cat,
}

function delete_cat(request) {
    return new Promise((resolve, reject) => {
        dishcategories.remove({ _id: request.id }).then(res => {
            const data = {
                status: 1,
                message: "Xóa thành công"
            }
            resolve(data)
        });
    })

}

function update_cat(pramas, request) {
    return new Promise((resolve, reject) => {

        dishcategories.findById({ _id: pramas.id }).then(res => {
            if (!res) {
                var err = {
                    message: "khong ton tai"
                }
                reject(err)
            } else if (res) {
                res.name = request.name || res.name
                res.description = request.description || res.description
                return res.save()
            }
        }).then(result => {
            const data = {
                message: "thanh cong",
                values: result
            }
            resolve(data);

        }).catch(err => {
            reject(err + "");
        })
    })

}

function get_cat_list() {
    return new Promise((resolve, reject) => {
        dishcategories.find({}).sort({ _id: -1 }).select('_id name name description').exec(
            function(err, response) {
                if (err) {
                    var err = {
                        err: err
                    }
                    reject(err)
                } else {
                    var data = response.map(res => {
                        return {
                            _id: res._id,
                            name: res.name,
                            description: res.description
                        }
                    })
                    resolve(data);
                }
            })
    });
}

function create_cat(request) {

    var newItem = new dishcategories({
        _id: new mongoose.Types.ObjectId(),
        name: request.name,
        description: request.description,
    });

    return new Promise((resolve, reject) => {
        var name = {
            name: new RegExp('^' + request.name.trim() + '$', "i")
        }
        dishcategories.find(name).then(items => {
            if (items.length > 0) {
                var err = {
                    message: "Đã tồn tại"
                }
                reject(err);
                return false;
            } else {
                console.log('daluu');

                return newItem.save()
            }
        }).then(result => {
            const data = {
                message: "luu thanh cong",
                values: {
                    _id: result._id,
                    name: result.name,
                    description: result.description
                }
            }
            resolve(data);
        }).catch(err => {
            var err = {
                err: err + ""
            }
            reject(err + "");
        })
    })
}