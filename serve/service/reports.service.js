var posts = require("../model/reports.model");
var mongoose = require("mongoose");
module.exports = {
    create_post: create_post,
    get_posts: get_posts,
    delete_post: delete_post,
    edit_post: edit_post,
    get_post_by_id: get_post_by_id
}

function get_post_by_id(req) {
    return new Promise((resolve, reject) => {
        posts.findOne({ _id: req.id }).select('_id title content created thumbnail_url').then(res => {
            if (!res) {
                var err = {
                    message: "Post not found"
                }
                reject(err)
            }
            var data = {
                _id: res._id,
                title: res.title,
                content: res.content,
                created: res.created,
                thumbnail_url: res.thumbnail_url
            }
            console.log(data);

            resolve(data)
        }).catch(err => reject(err + ""))
    });
}

function get_posts() {
    return new Promise((resolve, reject) => {
        posts.find({}).select('_id phone name content created').exec(
            function(err, response) {
                if (err) {
                    var err = {
                        err: err
                    }
                    reject(err)
                } else {
                    var data = response.map(res => {
                        return {
                            _id: res._id,
                            phone: res.phone,
                            name: res.name,
                            created: res.created,
                            content: res.content
                        }
                    })
                    resolve(data);
                }
            })
    });
}

function delete_post(request) {
    return new Promise((resolve, reject) => {
        posts.findOne({ _id: request.id }).then(res => {
            if (res) return posts.remove({ _id: request.id });
        }).then(res => {
            if (res) {
                var mes = {
                    status: 1,
                    message: "Deleted"
                }
                resolve(mes);
            }
        }).catch(err => {
            reject(err + "");
        })

    });
}

function edit_post(pramas, request) {
    return new Promise((resolve, reject) => {
        posts.findById({ _id: pramas.id }).then(res => {
            if (!res) {
                var err = {
                    message: "Not Found!"
                }
                reject(err)
            } else if (res) {
                res.title = request.title || res.title
                res.content = request.content || res.content
                res.thumbnail_url = request.thumbnail_url || res.thumbnail_url
                return res.save()
            }
        }).then(result => {
            const data = {
                message: "Cập nhật thành công",
                status: 200,
                values: {
                    title: result.title,
                    content: result.content,
                    thumbnail_url: result.thumbnail_url,
                }
            }
            resolve(data);
        }).catch(err => {
            reject(err + "");
        })
    })
}

function create_post(request) {

    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = (day) + "-" + (month) + "-" + now.getFullYear();


    var newPost = new posts({
        _id: new mongoose.Types.ObjectId(),
        phone: request.phone,
        name: request.name,
        content: request.content,
        created: today
    });

    return new Promise((resolve, reject) => {
        posts.find({ _id: newPost._id }).then(items => {
            if (items.length > 0) {

                var err = {
                    message: "Có lỗi xảy ra"
                }
                reject(err)
            } else {
                console.log(newPost.save());
                return newPost.save()
            }
        }).then(result => {
            resolve(newPost);
        }).catch(err => {
            var err = {
                err: err + ""
            }
            reject(err + "");
        })
    })
}