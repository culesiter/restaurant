import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/observable';
import { Router } from '@angular/router';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  ckeditorContent: string = '<p>Some html</p>';
  private url = environment.base_url + '/posts';
  private post_list : any[] = [];
  constructor(private http: Http, private router:Router) { }

  ngOnInit() {
    this.getPosts();
  }
  getPosts(){
    this.http.get(this.url).map(respose => respose.json() as any[]).subscribe(res => { 
      this.post_list = res;
      console.log(this.post_list);
     });
  }
  denTrangChiTiet(id)
  {
    this.router.navigate(['/home/news/'+id]);
  }
}
