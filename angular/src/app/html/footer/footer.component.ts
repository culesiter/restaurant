import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/observable';
import { environment } from '../../../environments/environment';
declare var $: any;
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  private report: any = {};
  private err_noti = false;
  private url = environment.base_url + '/reports';
  constructor(private http: Http) { }

  ngOnInit() {
    const current_customer = JSON.parse(localStorage.getItem('user'));
     this.report.phone = (current_customer) ? current_customer.sdt : '';
     this.report.name = (current_customer) ? current_customer.ten : '';
  }
  sendReport() {
    if (!this.report.phone || !this.report.name || !this.report.content) this.err_noti = true;
    else  {
      this.err_noti = false;
      this.http.post(this.url, this.report).map(res => res as any).subscribe(res => { 
        $.notify('Cảm ơn quý khách đã góp ý', 'success');
        this.report = {};
        setTimeout(() => {
          $('#report-modal').modal('hide');
        }, 200);
     });
    }
  }
}
