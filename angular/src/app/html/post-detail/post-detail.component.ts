import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/observable';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {
  private url = environment.base_url + '/posts';
  private post : any = {};
  constructor(private http: Http, private router: ActivatedRoute) { }

  ngOnInit() {
    this.getPost();
  }
 
  getPost() {
    this.router.params.subscribe(pramas => {
      const id = pramas['id'];
      this.http.get(this.url + '/' + id).map(respose => respose.json() as any[]).subscribe(res => { 
        this.post = res;
        console.log(this.post);
       });
    })
  }
}
