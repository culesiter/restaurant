import { Router } from '@angular/router';
import { ThucdonserviceService } from './../../share/services/thucdonservice.service';
import { Ithucdon } from './../../share/entities/ithucdon';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Idish } from './../../share/entities/idish';
import { DishserviceService } from './../../share/services/dishservice.service';
import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-dish-lish',
  templateUrl: './dish-lish.component.html',
  styleUrls: ['./dish-lish.component.scss'],

})
export class DishLishComponent implements OnInit {

  private dishs: Idish[];
  private dishs2: Idish[];
  private thucdons: Ithucdon[];
  private monans: Ithucdon[];
  private mo: number = 1;
  private frmTimKiem: FormGroup;
  private doDai: number = 0;
  private categories: any[] = [];
  private min_km: number = -1;
  private max_km: number = 100;
  constructor(private router: Router, private thucdonservice: ThucdonserviceService, private dishservice: DishserviceService, private formbuilder: FormBuilder) { }

  ngOnInit() {
    this.danhSachMonAn();
    if (window.location.pathname == "/home/dish/thucdon") {
      this.mo = 2;
    }
    this.get_list_type();
    this.doiTaiTrang();
    this.taoForm();
    this.showdishs();
    this.trinhThucDon();

  }

  get_list_type() {
    this.dishservice.laydsloaimon().subscribe(res => {
      console.log(res);
      this.categories = res;
      this.frmTimKiem.patchValue({loai: 0});
    });
  }

  denTrangChiTiet(id) {
    this.router.navigate(['/home/monan/' + id]);
  }
  danhSachMonAn() {
    this.thucdonservice.laydanhsachThucDon().subscribe(response => {
      this.monans = response;
    })

  }
  moTab1() {
    this.mo = 2;
  }
  moTab2() {
    this.mo = 1;
  }
  trinhThucDon() {
    this.thucdonservice.laydanhsachmonanTrungBay().subscribe(response => {
      this.thucdons = response;
    })
  }

  doiTaiTrang() {
    $(function () {
      $(".loader1").fadeOut(500, function () {
        $(".conten").fadeIn(3000);
      });
    })
  }
  taoForm() {
    this.frmTimKiem = this.formbuilder.group({
      khuyenmai: ['', []],
      loai: ['', []],
      min_km: ['0', []],
      max_km: ['100', []]
    })
  }
  kiemTraTop(mo) {
    window.onscroll = function () { myFunction() };
    function myFunction() {
      if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.getElementById("header").className = "display";
        document.getElementById("header1").className = "";


      } else {
        document.getElementById("header").className = "";
        document.getElementById("header1").className = "display";
      }
    }

  }
  showdishs() {
    this.dishservice.laydanhsachmonan().subscribe(response => {
      this.dishs = response;
      
      console.log(this.dishs);
    });
  }


  layGiaTri() {
    console.log(this.frmTimKiem.value);

    this.dishservice.getFilterDishes(this.frmTimKiem.value).subscribe(response => {
      this.dishs = response.map(res => {
        return res;
      })
    })
  }
}
