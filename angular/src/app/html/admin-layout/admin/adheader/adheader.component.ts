import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from '../../../../share/services/staff.service';
import { DataTransferService } from '../../../../share/services/DataTransfer/data-transfer.service';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/observable';
import { environment } from '../../../../../environments/environment';
declare var $: any;
@Component({
  selector: 'app-adheader',
  templateUrl: './adheader.component.html',
  styleUrls: ['./adheader.component.scss']
})
export class AdheaderComponent implements OnInit {
  constructor(private route: ActivatedRoute, private staff: StaffService,private router: Router, private http: Http,private dataTransferService: DataTransferService) { }
  private styleExp = 'none';
  private styleExp2 = 'none';
  private user : any = {};
  private allrank;
  private current_role;

  private report: any[] = [];
  private url = environment.base_url + '/reports';
  ngOnInit() {
    this.getcurrent_user();
  }
  getallrank() {
    this.staff.laydscapnv().subscribe(res => {
      this.allrank = res;
    });
  }
  getcurrent_user() {
    this.user = JSON.parse(sessionStorage.getItem('admin'));
    this.current_role = this.user.user._idcapnhanvien._id;
    console.log(this.current_role);
    
  }

  activeRouter(b) {
    this.dataTransferService.change(b);
  }
  activeRouter1(a) {
    this.dataTransferService.change(a);
    //  loai mon
    if (this.styleExp === 'none') {
      this.styleExp = 'block';
    } else {
      this.styleExp = 'none';
    }
    // end loai mon
    // open loai phong
    if (this.styleExp2 === 'block') {
      this.styleExp2 = 'none';
    }
    // end loai phong
  }
  activeRouter2(b) {
    this.dataTransferService.change(b);
    if (this.styleExp === 'block') {
      this.styleExp = 'none';
    }
    if (this.styleExp2 === 'none') {
      this.styleExp2 = 'block';
    } else {
      this.styleExp2 = 'none';
    }
  }

  getReport() {
    this.http.get(this.url).map(res => res as any).subscribe(res => { 
      this.report = JSON.parse(res._body);
      setTimeout(() => {
        $('#report-modal-show').modal('show');
      }, 200);
   });
  }
}
