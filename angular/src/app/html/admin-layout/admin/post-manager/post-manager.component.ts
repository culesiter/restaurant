import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/observable';
declare var $: any;
@Component({
  selector: 'app-post-manager',
  templateUrl: './post-manager.component.html',
  styleUrls: ['./post-manager.component.scss']
})
export class PostManagerComponent implements OnInit {
  private url = environment.base_url + '/posts';
  private post_list : any[] = [];
  private post_detail : any = {};
  private newPost : any = {};
  ckeditorContent: string = '<p>Some html</p>';
  constructor(private http: Http) { }

  ngOnInit() {
    this.getListType();
  }
  getListType() {
   this.http.get(this.url).map(respose => respose.json() as any[]).subscribe(res => { 
     this.post_list = res;
    });
  }
  openDetail(id) {
    this.http.get(this.url +'/' + id).map(respose => respose.json() as any[]).subscribe(res => { 
      this.post_detail = res;
     });
  }
  updateDetail() {
    console.log(1);
    return this.http.put(this.url + '/' + this.post_detail._id, this.post_detail).subscribe(res => { 
      if (res.status == 200) {
        $.notify('Cập nhật thành công', 'success');
      }
     });
  }
  create() {
      this.http.post(this.url, this.newPost).map(res => res as any).subscribe(res => { 
        $.notify('Tạo thành công', 'success');
        $('#report-modal').modal('hide');
     });
  }
}
