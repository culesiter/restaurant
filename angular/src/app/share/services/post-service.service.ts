import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';
@Injectable()
export class PostServiceService {
  private url = environment.base_url + '/posts';
  constructor(private http: Http) { }


  getPosts(): Observable<any[]> {
    return this.http.get(this.url).map(respose => respose.json() as any[]);
  }
}
