import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Icustomer } from '../entities/icustomer';
import { environment } from '../../../environments/environment';
@Injectable()
export class HumanService {
  private url = environment.base_url+"/khachhang";
  private urlk = environment.base_url+"/khachhang/noaccount";
  private urlnv = environment.base_url+"/nhanvien/login";
  constructor(private http: Http) { }
  upanhkh(id, img): Observable<any> {
    return this.http.put(this.url + '/img/' + id, img).map(res => {
      return res.json();
    });
  }
  them1(data): Observable<any> {
    return this.http.post(this.url, data).map(res => {
      console.log(res);
      return res.json();
    });
  }
  themtknoaccount(data): Observable<any> {
    return this.http.post(this.urlk, data).map(res => {
      console.log(res);
      return res.json();
    });
  }
  them(data): Observable<any> {
    return this.http.post(this.url, data).map(res => {
      return res.json();
    });
  }
  xoa(id): Observable<Icustomer> {
    return this.http.delete(this.url + '/' + id).map(res => res.json() as Icustomer);
  }
  sua(id, data): Observable<Icustomer> {
    return this.http.put(this.url + '/' + id, data).map(res => res.json() as Icustomer);
  }
  laydanhsach(): Observable<Icustomer[]> {
    return this.http.get(this.url).map(respose => respose.json() as Icustomer[]);
  }

  // dangnhap
  login(user): Observable<any> {
    return this.http.post(this.urlnv, user).map(respose => respose.json());
  }
  laythongtinkhachhangtheoid(id): Observable<any> {
    return this.http.get(environment.base_url+'/khachhang/id?id=' + id).map(res => res.json());
  }
}
