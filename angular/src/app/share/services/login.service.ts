import { Injectable } from '@angular/core';
import { Idish } from './../entities/idish';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';
@Injectable()
export class LoginService {
  private loginurl = environment.base_url+'/khachhang/login';
  private hoadon = environment.base_url+'/hoadon/gettime';
  private hoadon1 = environment.base_url+'/hoadon';
  private cthd = environment.base_url+'/chitiethoadon';
  private payment_url = environment.base_url+'/vnp/create_payment_url';
  public _islogin: boolean;
  constructor(private http: Http) { }

  islogin(): boolean {
    return this._islogin;
  }


  setislogin(islogin: boolean) {
    this._islogin = islogin;
  }

  gettime(time, Idp): Observable<any> {
    const date = {
      thoidiemden: time,
      _idphong: Idp
    };
    return this.http.post(this.hoadon, date).map(respose => respose.json());
  }
  getlistblankroom(time): Observable<any> {
    const date = {
      thoidiemden: time
    };
    return this.http.post(this.hoadon, date).map(respose => respose.json());
  }
  login(email, matkhau): Observable<any> {
    const user = {
      email: email,
      matkhau: matkhau
    };
    return this.http.post(this.loginurl, user).map(respose => respose.json());
  }
  themhoadon(data): Observable<any> {
    return this.http.post(this.hoadon1, data).map(res => {
      return res.json();
    });
  }
  themcthd(data): Observable<any> {
    return this.http.post(this.cthd, data).map(res => {
      return res.json();
    });
  }
  thanhtoan(data): Observable<any> {
    return this.http.post(this.payment_url, data).map(res => {
      return res;
    });
  }
  checkthanhtoan(query): Observable<any> {
    console.log(query);
    return this.http.get('https://manhrestaurant.herokuapp.com/payment/callback' + query).map(res => {
      return res.json();
    });
  }
}